## Scala Project
This project was written using Scala with SBT.

This project concerns quotations of currency rates downloaded from the API of the National Bank of Poland.
Its functionalities: 
* receiving currency rates (for a specific currency or for all available currencies): current, past, from some range of days or some recent ones
* displaying tables and charts with quotations
* possibility of sorting the table according to different parameters 
* ability to calculate arithmetic mean, median and standard deviation
* calculator allowing to change one currency into another on the basis of the current exchange rate or the exchange rate of a particular day

### Install
1. Open the project in the [IntelliJ IDEA](https://www.jetbrains.com/idea/) environment by selecting *build.sbt* 
2. Build a project 
3. Run the project through *MainView.scala*, included in the *View* package

### Sample screenshots
![](https://gitlab.com/marcinkwapisz/scalaproject/uploads/3a82c6aa567d5e41aba816e7b17ba967/1.PNG)
![](https://gitlab.com/marcinkwapisz/scalaproject/uploads/7f395b4c0bf9af4b2de4cc4ff57efd90/2.PNG)
![](https://gitlab.com/marcinkwapisz/scalaproject/uploads/4791b930c7f398821519e3301cdec994/3.PNG)
