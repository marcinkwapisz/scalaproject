package View

import java.time.LocalDate

import Data._
import Service.{DataFetcherNBP, ShowDialog, StatisticsCalculator}
import scalafx.beans.property.{ObjectProperty, StringProperty}
import scalafx.collections.ObservableBuffer
import scalafx.geometry.{Insets, Orientation, Pos}
import scalafx.scene.chart.{BarChart, CategoryAxis, NumberAxis, XYChart}
import scalafx.scene.control._
import scalafx.scene.effect.DropShadow
import scalafx.scene.layout.{HBox, VBox}

import scala.util.Try

object AllCurrenciesView {
  private var typeOfRequest = ""
  private var dateFrom = ""
  private var dateTo = ""
  private val rates = ObservableBuffer[RateCCM]()

  private val chooseTypeOfRequestLabel = new Label {
    text = UIControlTexts.chooseTypeOfRequest
  }

  private val singleDatePicker = new DatePicker() {
    disable = true
    promptText = UIControlTexts.enterDate
    onAction = _ => {
      value.value match {
        case value if value.isAfter(LocalDate.now()) =>
          ShowDialog.showWarningAboutDate(DialogStrings.dateFromFuture)
          style = Styles.red
        case _ =>
          typeOfRequest = value.value.toString
          style = Styles.none
      }
    }
  }

  private val startDatePicker = new DatePicker() {
    disable = true
    promptText = UIControlTexts.enterStartDate
    onAction = _ => {
      value.value match {
        case value if value.isAfter(LocalDate.now()) =>
          ShowDialog.showWarningAboutDate(DialogStrings.dateFromFuture)
          style = Styles.red
        case _ =>
          dateFrom = value.value.toString + "/"
          typeOfRequest = dateFrom + dateTo
          endDatePicker.disable = false
          style = Styles.none
      }
    }
  }

  private val endDatePicker = new DatePicker() {
    disable = true
    promptText = UIControlTexts.enterEndDate
    onAction = _ => {
      value.value match {
        case value if value.isAfter(LocalDate.now()) =>
          ShowDialog.showWarningAboutDate(DialogStrings.dateFromFuture)
          style = Styles.red
        case value if value.isBefore(LocalDate.parse(dateFrom.substring(0, 10))) =>
          ShowDialog.showWarningAboutDate(DialogStrings.endDateBeforeStartDate)
          style = Styles.red
        case _ =>
          dateTo = value.value.toString
          typeOfRequest = dateFrom + dateTo
          style = Styles.none
      }
    }
  }

  private val numberOfQuotesPicker = new TextField {
    disable = true
    style = Styles.center
    promptText = UIControlTexts.enterNumberOfQuotes
    maxWidth = 175
    text.onChange {
      Try(text.apply.toInt).toOption match {
        case Some(value) =>
          value match {
            case value if value > 67 =>
              ShowDialog.showWarning(DialogStrings.intNumberLessOrEqual67, DialogStrings.intNumber67Limit)
              style = Styles.redCenter
            case value if value < 1 =>
              ShowDialog.showWarning(DialogStrings.intNumberGreaterThanZero, DialogStrings.numberOfQuotesLessThanZero)
              style = Styles.redCenter
            case _ =>
              typeOfRequest = "/last/" + text.apply
              style = Styles.noneCenter
          }
        case None =>
          ShowDialog.showWarning(DialogStrings.intNumberRequired, DialogStrings.noIntNumberEntered)
          style = Styles.redCenter
      }
    }
  }

  private val typeOfRequestChoiceBox = new ChoiceBox[String] {
    items = ObservableBuffer(UIControlTexts.typesOfRequestStrings)
    value.onChange {
      value.value match {
        case value if value.equals(items.get().get(0)) =>
          typeOfRequest = ""
          singleDatePicker.disable = true
          startDatePicker.disable = true
          endDatePicker.disable = true
          numberOfQuotesPicker.disable = true
        case value if value.equals(items.get().get(1)) =>
          singleDatePicker.disable = false
          startDatePicker.disable = true
          endDatePicker.disable = true
          numberOfQuotesPicker.disable = true
        case value if value.equals(items.get().get(2)) =>
          singleDatePicker.disable = true
          startDatePicker.disable = false
          endDatePicker.disable = true
          numberOfQuotesPicker.disable = true
        case value if value.equals(items.get().get(3)) =>
          singleDatePicker.disable = true
          startDatePicker.disable = true
          endDatePicker.disable = true
          numberOfQuotesPicker.disable = false
      }
    }
    selectionModel().selectFirst()
  }

  private val labelAboveTable = Label("")
  labelAboveTable.setStyle(Styles.bold)

  private val ratesTableView: TableView[RateCCM] = new TableView[RateCCM] {
    prefHeight = 300
    prefWidth = 450
    val currencyColumn = new TableColumn[RateCCM, String](text = "Waluta")
    currencyColumn.cellValueFactory = cvf => StringProperty(cvf.value.currency)
    currencyColumn.prefWidth = 140
    currencyColumn.setStyle(Styles.center)
    val codeColumn = new TableColumn[RateCCM, String](text = "Kod")
    codeColumn.cellValueFactory = cvf => StringProperty(cvf.value.code)
    codeColumn.prefWidth = 75
    codeColumn.setStyle(Styles.center)
    val midColumn = new TableColumn[RateCCM, Float](text = "Średnia wartość")
    midColumn.cellValueFactory = cvf => ObjectProperty(cvf.value.mid)
    midColumn.prefWidth = 115
    midColumn.setStyle(Styles.center)
    val dateColumn = new TableColumn[RateCCM, String](text = "Data")
    dateColumn.cellValueFactory = cvf => StringProperty(cvf.value.date)
    dateColumn.prefWidth = 100
    dateColumn.setStyle(Styles.center)
    columns ++= List(currencyColumn, codeColumn, midColumn, dateColumn)
  }

  private val ratesChart = BarChart(CategoryAxis("Waluta"), NumberAxis("PLN"))
  ratesChart.animated = false

  private val getStackQuotesButton = new Button {
    text = UIControlTexts.getStackQuotes
    effect = new DropShadow()
    onAction = _ => {
      val connectionUrl = "tables/a/" + typeOfRequest + "/?format=json"
      val arrayOfRootObjectTNER: Array[RootObjectTNER] = DataFetcherNBP.getArrayOfRootObjectTNER(connectionUrl)
      if (arrayOfRootObjectTNER != null) {
        val ratesFromArray = arrayOfRootObjectTNER.map(_.rates)
        for (rootObjectTNER <- arrayOfRootObjectTNER;
             rateCCM <- rootObjectTNER.rates) {
          rateCCM.date = rootObjectTNER.effectiveDate
        }
        rates.clear()
        for (rate <- ratesFromArray) {
          rates ++= ObservableBuffer[RateCCM](rate)
        }
        ratesTableView.items = rates
        updateLabelAboveTable(arrayOfRootObjectTNER)
        updateChartSeries(arrayOfRootObjectTNER)
        calculateStatisticsOfRates()
      }
    }
  }

  private val medianLabel = new Label {
    text = UIControlTexts.median
    style = Styles.bold
  }

  private val medianTextField = new TextField {
    style = Styles.center
    editable = false
  }

  private val meanLabel = new Label {
    text = UIControlTexts.mean
    style = Styles.bold
  }

  private val meanTextField = new TextField {
    editable = false
    style = Styles.center
  }

  private val standardDeviationLabel = new Label {
    text = UIControlTexts.standardDeviation
    style = Styles.bold
  }

  private val standardDeviationTextField = new TextField {
    editable = false
    style = Styles.center
  }

  private val selectionContent = new VBox {
    padding = Insets(5)
    spacing = 10
    alignment = Pos.Center
    children = List(chooseTypeOfRequestLabel, typeOfRequestChoiceBox, singleDatePicker, startDatePicker,
      endDatePicker, numberOfQuotesPicker, new Separator(), getStackQuotesButton)
  }

  private val tableContent = new VBox {
    padding = Insets(10)
    spacing = 5
    alignment = Pos.Center
    children = List(labelAboveTable, ratesTableView)
  }

  private val statisticsContent = new VBox {
    padding = Insets(5)
    spacing = 10
    alignment = Pos.Center
    children = List(medianLabel, medianTextField, meanLabel, meanTextField, standardDeviationLabel,
      standardDeviationTextField)
  }

  private val upperPartOfContent = new HBox() {
    spacing = 30
    children = List(selectionContent, new Separator {
      orientation = Orientation.Vertical
    }, tableContent, new Separator {
      orientation = Orientation.Vertical
    }, statisticsContent)
  }

  private val chartContent = new VBox {
    padding = Insets(5)
    spacing = 10
    alignment = Pos.Center
    children = List(new Separator(), ratesChart)
  }

  val allCurrenciesContent: VBox = new VBox {
    padding = Insets(5)
    spacing = 10
    alignment = Pos.Center
    children = List(upperPartOfContent, chartContent)
  }

  private def updateChartSeries(arrayOfRootObjectTNER: Array[RootObjectTNER]): Unit = {
    ratesChart.getData.clear()
    for (rootObjectTNER <- arrayOfRootObjectTNER) {
      val chartSeries = XYChart.Series(rootObjectTNER.effectiveDate, ObservableBuffer(rootObjectTNER.rates
        .map(r => XYChart.Data(r.code, r.mid: Number)): _*))
      ratesChart.getData.add(chartSeries)
    }
  }

  private def updateLabelAboveTable(arrayOfRootObjectTNER: Array[RootObjectTNER]): Unit = {
    labelAboveTable.text = "Tabela: " + arrayOfRootObjectTNER.headOption.get.table
  }

  private def calculateStatisticsOfRates(): Unit = {
    val results: Seq[String] = StatisticsCalculator.calculateRatesStatistics(rates.map(_.mid))
    medianTextField.text = results.head
    meanTextField.text = results(1)
    standardDeviationTextField.text = results(2)
  }
}
