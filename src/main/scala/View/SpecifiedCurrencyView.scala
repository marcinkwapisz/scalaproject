package View

import java.time.LocalDate

import Data.{DialogStrings, RateCCM, RateNEM, RootObjectTCCR, RootObjectTNER, Styles, UIControlTexts}
import Service.{DataFetcherNBP, ShowDialog, StatisticsCalculator}
import scalafx.beans.property.{ObjectProperty, StringProperty}
import scalafx.collections.ObservableBuffer
import scalafx.geometry.{Insets, Orientation, Pos}
import scalafx.scene.chart.{CategoryAxis, LineChart, NumberAxis, XYChart}
import scalafx.scene.control._
import scalafx.scene.effect.DropShadow
import scalafx.scene.layout.{HBox, VBox}

import scala.collection.mutable
import scala.util.Try

object SpecifiedCurrencyView {
  private var currencyCode = ""
  private var typeOfRequest = ""
  private var dateFrom = ""
  private var dateTo = ""
  private val rates = ObservableBuffer[RateNEM]()

  private val chooseCurrencyLabel = new Label {
    text = UIControlTexts.chooseCurrency
  }

  private val currencyChoiceBox = new ChoiceBox[String] {
    items = getCurrenciesLabels
    value.onChange {
      currencyCode = value.value.substring(0, 3).toLowerCase
    }
    selectionModel().selectFirst()
  }

  private val singleDatePicker = new DatePicker() {
    disable = true
    promptText = UIControlTexts.enterDate
    onAction = _ => {
      value.value match {
        case value if value.isAfter(LocalDate.now()) =>
          ShowDialog.showWarningAboutDate(DialogStrings.dateFromFuture)
          style = Styles.red
        case _ =>
          typeOfRequest = value.value.toString
          style = Styles.none
      }
    }
  }

  private val startDatePicker = new DatePicker() {
    disable = true
    promptText = UIControlTexts.enterStartDate
    onAction = _ => {
      value.value match {
        case value if value.isAfter(LocalDate.now()) =>
          ShowDialog.showWarningAboutDate(DialogStrings.dateFromFuture)
          style = Styles.red
        case _ =>
          dateFrom = value.value.toString + "/"
          typeOfRequest = dateFrom + dateTo
          endDatePicker.disable = false
          style = Styles.none
      }
    }
  }

  private val endDatePicker = new DatePicker() {
    disable = true
    promptText = UIControlTexts.enterEndDate
    onAction = _ => {
      value.value match {
        case value if value.isAfter(LocalDate.now()) =>
          ShowDialog.showWarningAboutDate(DialogStrings.dateFromFuture)
          style = Styles.red
        case value if value.isBefore(LocalDate.parse(dateFrom.substring(0, 10))) =>
          ShowDialog.showWarningAboutDate(DialogStrings.endDateBeforeStartDate)
          style = Styles.red
        case _ =>
          dateTo = value.value.toString
          typeOfRequest = dateFrom + dateTo
          style = Styles.none
      }
    }
  }

  private val numberOfQuotesPicker = new TextField {
    disable = true
    style = Styles.center
    promptText = UIControlTexts.enterNumberOfQuotes
    maxWidth = 175
    text.onChange {
      Try(text.apply.toInt).toOption match {
        case Some(value) =>
          value match {
            case value if value > 255 =>
              ShowDialog.showWarning(DialogStrings.intNumberLessOrEqual255, DialogStrings.intNumber255Limit)
              style = Styles.redCenter
            case value if value < 1 =>
              ShowDialog.showWarning(DialogStrings.intNumberGreaterThanZero, DialogStrings.numberOfQuotesLessThanZero)
              style = Styles.redCenter
            case _ =>
              typeOfRequest = "/last/" + text.apply
              style = Styles.noneCenter
          }
        case None =>
          ShowDialog.showWarning(DialogStrings.intNumberRequired, DialogStrings.noIntNumberEntered)
          style = Styles.redCenter
      }
    }
  }

  private val chooseTypeOfRequestLabel = new Label {
    text = UIControlTexts.chooseTypeOfRequest
  }

  private val typeOfRequestChoiceBox = new ChoiceBox[String] {
    items = ObservableBuffer(UIControlTexts.typesOfRequestStrings)
    value.onChange {
      value.value match {
        case value if value.equals(items.get().get(0)) =>
          typeOfRequest = ""
          singleDatePicker.disable = true
          startDatePicker.disable = true
          endDatePicker.disable = true
          numberOfQuotesPicker.disable = true
        case value if value.equals(items.get().get(1)) =>
          singleDatePicker.disable = false
          startDatePicker.disable = true
          endDatePicker.disable = true
          numberOfQuotesPicker.disable = true
        case value if value.equals(items.get().get(2)) =>
          singleDatePicker.disable = true
          startDatePicker.disable = false
          endDatePicker.disable = true
          numberOfQuotesPicker.disable = true
        case value if value.equals(items.get().get(3)) =>
          singleDatePicker.disable = true
          startDatePicker.disable = true
          endDatePicker.disable = true
          numberOfQuotesPicker.disable = false
      }
    }
    selectionModel().selectFirst()
  }

  private val labelAboveTable = Label("")
  labelAboveTable.setStyle(Styles.bold)

  private val ratesTableView: TableView[RateNEM] = new TableView[RateNEM] {
    prefHeight = 300
    prefWidth = 450
    val numberColumn = new TableColumn[RateNEM, String](text = "Numer")
    numberColumn.cellValueFactory = cvf => StringProperty(cvf.value.no)
    numberColumn.prefWidth = 150
    numberColumn.setStyle(Styles.center)
    val dateColumn = new TableColumn[RateNEM, String](text = "Data")
    dateColumn.cellValueFactory = cvf => StringProperty(cvf.value.effectiveDate)
    dateColumn.prefWidth = 140
    dateColumn.setStyle(Styles.center)
    val midColumn = new TableColumn[RateNEM, Float](text = "Średnia wartość")
    midColumn.cellValueFactory = cvf => ObjectProperty(cvf.value.mid)
    midColumn.prefWidth = 140
    midColumn.setStyle(Styles.center)
    columns ++= List(numberColumn, dateColumn, midColumn)
  }

  private val ratesChart = LineChart(CategoryAxis("Data"), NumberAxis("PLN"))
  ratesChart.animated = false
  ratesChart.legendVisible = false

  private val getStackQuotesButton = new Button {
    text = UIControlTexts.getStackQuotes
    effect = new DropShadow()
    onAction = _ => {
      val connectionUrl = "rates/a/" + currencyCode + "/" + typeOfRequest + "/?format=json"
      val rootObjectTCCR: RootObjectTCCR = DataFetcherNBP.getRootObjectTCCR(connectionUrl)
      if (rootObjectTCCR != null) {
        rates.clear()
        for (rate <- rootObjectTCCR.rates) {
          rates ++= ObservableBuffer[RateNEM](rate)
        }
        ratesTableView.items = rates
        updateLabelAboveTable(rootObjectTCCR)
        updateChartSeries()
        calculateStatisticsOfRates()
      }
    }
  }

  private val medianLabel = new Label {
    text = UIControlTexts.median
    style = Styles.bold
  }

  private val medianTextField = new TextField {
    style = Styles.center
    editable = false
  }

  private val meanLabel = new Label {
    text = UIControlTexts.mean
    style = Styles.bold
  }

  private val meanTextField = new TextField {
    editable = false
    style = Styles.center
  }

  private val standardDeviationLabel = new Label {
    text = UIControlTexts.standardDeviation
    style = Styles.bold
  }

  private val standardDeviationTextField = new TextField {
    editable = false
    style = Styles.center
  }

  private val selectionContent = new VBox {
    padding = Insets(5)
    spacing = 10
    alignment = Pos.Center
    children = List(chooseCurrencyLabel, currencyChoiceBox, new Separator(), chooseTypeOfRequestLabel,
      typeOfRequestChoiceBox, singleDatePicker, startDatePicker, endDatePicker, numberOfQuotesPicker,
      new Separator(), getStackQuotesButton)
  }

  private val tableContent = new VBox {
    padding = Insets(10)
    spacing = 5
    alignment = Pos.Center
    children = List(labelAboveTable, ratesTableView)
  }

  private val statisticsContent = new VBox {
    padding = Insets(5)
    spacing = 10
    alignment = Pos.Center
    children = List(medianLabel, medianTextField, meanLabel, meanTextField, standardDeviationLabel,
      standardDeviationTextField)
  }

  private val upperPartOfContent = new HBox() {
    spacing = 20
    children = List(selectionContent, new Separator {
      orientation = Orientation.Vertical
    }, tableContent, new Separator {
      orientation = Orientation.Vertical
    }, statisticsContent)
  }

  private val chartContent = new VBox {
    padding = Insets(5)
    spacing = 10
    alignment = Pos.Center
    children = List(new Separator(), ratesChart)
  }

  val specifiedCurrencyContent: VBox = new VBox {
    padding = Insets(5)
    spacing = 10
    alignment = Pos.Center
    children = List(upperPartOfContent, chartContent)
  }

  private def getCurrenciesLabels: ObservableBuffer[String] = {
    val urlToCurrencyNames = "tables/a/"
    val arrayOfRootObjectTNER: Array[RootObjectTNER] = DataFetcherNBP.getArrayOfRootObjectTNER(urlToCurrencyNames)
    val ratesFromArray: Array[RateCCM] = arrayOfRootObjectTNER.map(_.rates).headOption.get
    val labels: mutable.MutableList[String] = mutable.MutableList()

    for (rate <- ratesFromArray) {
      labels += (rate.code + " - " + rate.currency)
    }
    ObservableBuffer(labels)
  }

  private def updateChartSeries(): Unit = {
    val chartSeries = XYChart.Series(ObservableBuffer(rates.map(r =>
      XYChart.Data(r.effectiveDate, r.mid: Number)): _*))
    ratesChart.getData.setAll(chartSeries)
  }

  private def updateLabelAboveTable(rootObjectTCCR: RootObjectTCCR): Unit = {
    labelAboveTable.text = "Tabela: " + rootObjectTCCR.table + ", waluta: " + rootObjectTCCR.currency +
      ", kod: " + rootObjectTCCR.code
  }

  private def calculateStatisticsOfRates(): Unit = {
    val results: Seq[String] = StatisticsCalculator.calculateRatesStatistics(rates.map(_.mid))
    medianTextField.text = results.head
    meanTextField.text = results(1)
    standardDeviationTextField.text = results(2)
  }
}
