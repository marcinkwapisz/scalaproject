package View

import java.time.LocalDate

import Data.{DialogStrings, Styles, UIControlTexts}
import Service.{DataFetcherNBP, ShowDialog}
import scalafx.collections.ObservableBuffer
import scalafx.geometry.Pos
import scalafx.scene.control._
import scalafx.scene.effect.DropShadow
import scalafx.scene.layout.VBox

import scala.collection.mutable
import scala.util.Try

object CurrencyCalculatorView {
  private var amountOfMoney = 0.0
  private val labels = getCurrenciesLabels
  private var firstCurrency = ""
  private var secondCurrency = ""
  private var date = LocalDate.now()

  private val amountOfMoneyLabel = new Label {
    text = UIControlTexts.amountOfMoney
  }

  private val amountOfMoneyPicker = new TextField {
    style = Styles.center
    maxWidth = 260
    text.onChange {
      Try(text.apply.toDouble).toOption match {
        case Some(value) =>
          value match {
            case value if value < 0 =>
              ShowDialog.showWarning(DialogStrings.nonNegativeAmountOfMoney, DialogStrings.negativeAmountOfMoney)
              style = Styles.redCenter
            case _ =>
              amountOfMoney = text.apply.toDouble
              style = Styles.noneCenter
          }
        case None =>
          ShowDialog.showWarning(DialogStrings.amountOfMoneyWithDotSeparator, DialogStrings.noAmountOfMoneyEntered)
          style = Styles.redCenter
      }
    }
  }

  private val amountOfMoneyContent = new VBox {
    spacing = 5
    alignment = Pos.Center
    children = List(amountOfMoneyLabel, amountOfMoneyPicker)
  }

  private val chooseFirstCurrencyLabel = new Label {
    text = UIControlTexts.chooseFirstCurrency
  }

  private val firstCurrencyChoiceBox = new ChoiceBox[String] {
    items = labels
    items.get().add(0, "PLN - nowy polski złoty")
    value.onChange {
      firstCurrency = value.value.substring(0, 3).toLowerCase
    }
    selectionModel().selectFirst()
  }

  private val chooseSecondCurrencyLabel = new Label {
    text = UIControlTexts.chooseSecondCurrency
  }

  private val secondCurrencyChoiceBox = new ChoiceBox[String] {
    items = labels
    value.onChange {
      secondCurrency = value.value.substring(0, 3).toLowerCase
    }
    selectionModel().select(2)
  }

  private val choiceCurrencyContent = new VBox {
    spacing = 5
    alignment = Pos.Center
    children = List(chooseFirstCurrencyLabel, firstCurrencyChoiceBox, chooseSecondCurrencyLabel,
      secondCurrencyChoiceBox)
  }

  private val chooseDateLabel = new Label {
    text = UIControlTexts.chooseDate
  }

  private val datePicker = new DatePicker(date) {
    onAction = _ => {
      value.value match {
        case value if value.isAfter(LocalDate.now()) =>
          ShowDialog.showWarningAboutDate(DialogStrings.dateFromFuture)
          style = Styles.red
        case _ =>
          date = value.value
          style = Styles.none
      }
    }
  }

  private val choiceDateContent = new VBox {
    spacing = 5
    alignment = Pos.Center
    children = List(chooseDateLabel, datePicker)
  }

  private val resultTextField = new TextField {
    style = Styles.center
    editable = false
    visible = false
    maxWidth = 260
  }

  private val infoTextField = new Label {
    style = Styles.center
    text = ""
  }

  private val calculateButton = new Button {
    text = UIControlTexts.calculate
    effect = new DropShadow()
    onAction = _ => {
      resultTextField.text = calculate + " " + secondCurrency.toUpperCase()
      resultTextField.visible = true
    }
  }

  private val resultsContent = new VBox {
    spacing = 5
    alignment = Pos.Center
    children = List(resultTextField, infoTextField)
  }

  val currencyCalculatorContent: VBox = new VBox {
    spacing = 50
    alignment = Pos.Center
    children = List(amountOfMoneyContent, choiceCurrencyContent, choiceDateContent, calculateButton, resultsContent)
  }

  private def getCurrenciesLabels: ObservableBuffer[String] = {
    val urlToCurrencyNames = "tables/a/"
    val arrayOfRootObjectTNER = DataFetcherNBP.getArrayOfRootObjectTNER(urlToCurrencyNames)
    val ratesFromArray = arrayOfRootObjectTNER.map(_.rates).headOption.get
    val labels: mutable.MutableList[String] = mutable.MutableList()

    for (rate <- ratesFromArray) {
      labels += (rate.code + " - " + rate.currency)
    }
    ObservableBuffer(labels)
  }

  private def calculate: String = {
    var firstFactor = 0.0
    var secondFactor = 0.0

    if (firstCurrency.equals("pln")) {
      firstFactor = 1.0
    }
    else {
      val firstConnectionUrl = "rates/a/" + firstCurrency + "/" + date + "/?format=json"
      val firstRootObjectTCCR = DataFetcherNBP.getRootObjectTCCR(firstConnectionUrl)
      if (firstRootObjectTCCR != null)
        firstFactor = firstRootObjectTCCR.rates.headOption.get.mid
    }

    if (secondCurrency.equals("pln")) {
      secondFactor = 1.0
    } else {
      val secondConnectionUrl = "rates/a/" + secondCurrency + "/" + date + "/?format=json"
      val secondRootObjectTCCR = DataFetcherNBP.getRootObjectTCCR(secondConnectionUrl)
      if (secondRootObjectTCCR != null)
        secondFactor = secondRootObjectTCCR.rates.headOption.get.mid
    }

    try {
      val result = BigDecimal.decimal(amountOfMoney * firstFactor / secondFactor)
        .setScale(2, BigDecimal.RoundingMode.HALF_UP).toString()
      infoTextField.text = "\t\t\t1 " + firstCurrency.toUpperCase + " = " +
        BigDecimal.decimal(firstFactor / secondFactor).setScale(4, BigDecimal.RoundingMode.HALF_UP).toString() +
        " " + secondCurrency.toUpperCase() + "\nwedług średniego kursu NBP z dnia " + date
      result
    } catch {
      case _: NumberFormatException => ""
    }
  }
}
