package View

import scalafx.application.JFXApp
import scalafx.geometry.{Insets, Side}
import scalafx.scene.Scene
import scalafx.scene.control.TabPane.TabClosingPolicy
import scalafx.scene.control._
import scalafx.scene.layout.{Priority, VBox}

object MainView extends JFXApp {
  stage = new JFXApp.PrimaryStage {
    scene = new Scene(width = 1024, height = 768) {
      root = new VBox {
        vgrow = Priority.Always
        hgrow = Priority.Always
        spacing = 10
        padding = Insets(20)
        children = List(
          new TabPane {
            tabs = Seq(
              new Tab {
                text = "Wszystkie waluty"
                content = AllCurrenciesView.allCurrenciesContent
              },
              new Tab {
                text = "Konkretna waluta"
                content = SpecifiedCurrencyView.specifiedCurrencyContent
              },
              new Tab {
                text = "Kalkulator walut"
                content = CurrencyCalculatorView.currencyCalculatorContent
              }
            )
            tabClosingPolicy = TabClosingPolicy.Unavailable
            side = Side.Top
          }
        )
      }
    }
  }
}
