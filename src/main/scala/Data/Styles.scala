package Data

object Styles {
  val center = "-fx-alignment: CENTER;"
  val bold = "-fx-font-weight: bold"
  val red = "-fx-border-color: red;"
  val none = "-fx-border-color: none;"
  val redCenter = "-fx-border-color: red; -fx-alignment: CENTER;"
  val noneCenter = "-fx-border-color: none; -fx-alignment: CENTER;"
}
