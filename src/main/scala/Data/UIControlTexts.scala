package Data

object UIControlTexts {
  val chooseCurrency = "Wybierz walutę:"
  val enterDate = "Podaj datę"
  val enterStartDate = "Podaj datę początkową"
  val enterEndDate = "Podaj datę końcową"
  val enterNumberOfQuotes = "Podaj liczbę notowań"
  val typesOfRequestStrings = List("Aktualne", "Przeszłe", "Zakres dat", "Ileś ostatnich")
  val chooseTypeOfRequest = "Wybierz typ zapytania:"
  val getStackQuotes = "Pobierz notowania"
  val median = "Mediana:"
  val mean = "Średnia arytmeryczna:"
  val standardDeviation = "Odchylenie standardowe:"
  val amountOfMoney = "Kwota pieniędzy:"
  val chooseFirstCurrency = "Wybierz walutę, którą chcesz zamienić:"
  val chooseSecondCurrency = "Wybierz walutę, na którą chcesz zamienić:"
  val chooseDate = "Wybierz datę kursu:"
  val calculate = "Przelicz"
}
