package Service

import scalafx.collections.ObservableBuffer

object StatisticsCalculator {
  def calculateRatesStatistics(ratesMid: ObservableBuffer[Float]): Seq[String] = {
    val mean = ratesMid.sum / ratesMid.length
    val standardDeviation = calculateStandardDeviation(ratesMid, mean)
    val median = calculateMedian(ratesMid)

    val meanText = BigDecimal.decimal(mean).setScale(4, BigDecimal.RoundingMode.HALF_UP).toString
    var standardDeviationText = "NaN"
    if (ratesMid.length > 1)
      standardDeviationText = BigDecimal.decimal(standardDeviation)
        .setScale(4, BigDecimal.RoundingMode.HALF_UP).toString
    val medianText = BigDecimal.decimal(median)
      .setScale(4, BigDecimal.RoundingMode.HALF_UP).toString
    Seq(medianText, meanText, standardDeviationText)
  }

  private def calculateStandardDeviation(rates: Seq[Float], mean: Float): Double = {
    val variance = rates.map(mid => (mid - mean) * (mid - mean))
    val standardDeviation = Math.sqrt(variance.sum / (rates.length - 1))
    standardDeviation
  }

  private def calculateMedian(rates: Seq[Float]): Float = {
    val sortedSeq = rates.sorted
    if (rates.size % 2 == 1) sortedSeq(sortedSeq.size / 2)
    else {
      val (up, down) = sortedSeq.splitAt(rates.size / 2)
      (up.last + down.head) / 2
    }
  }
}
