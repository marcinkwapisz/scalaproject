package Service

import java.io.{FileNotFoundException, IOException}
import java.net.SocketTimeoutException

import Data.{DialogStrings, RootObjectTCCR, RootObjectTNER}
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import com.fasterxml.jackson.module.scala.experimental.ScalaObjectMapper
import scalafx.application.Platform

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
import scala.concurrent.{Await, Future}

object DataFetcherNBP {
  private val nbpApiUrl = "http://api.nbp.pl/api/exchangerates/"
  private val objectMapper = new ObjectMapper() with ScalaObjectMapper
  objectMapper.registerModule(DefaultScalaModule)

  def getRootObjectTCCR(url: String): RootObjectTCCR = {
    val eventualRootObjectTCCR: Future[RootObjectTCCR] = getContentFromUrl(nbpApiUrl + url)
      .map(value => objectMapper.readValue[RootObjectTCCR](value)).recover {
      case _: FileNotFoundException =>
        Platform.runLater(ShowDialog.showError(DialogStrings.checkEnteredDateAndTryAgain,
          DialogStrings.noQuotesFoundForGivenParameters))
        null
      case _: IOException =>
        Platform.runLater(ShowDialog.showError(DialogStrings.reduceRangeOfDaysAndTryAgain,
          DialogStrings.rangeDays367Limit))
        null
      case _: SocketTimeoutException =>
        Platform.runLater(ShowDialog.showError(DialogStrings.tryAgain,
          DialogStrings.connectionTimeout))
        null
    }
    Await.result(eventualRootObjectTCCR, 5000 milliseconds)
  }

  def getArrayOfRootObjectTNER(url: String): Array[RootObjectTNER] = {
    val eventualRootObjectTNERArray: Future[Array[RootObjectTNER]] = getContentFromUrl(nbpApiUrl + url)
      .map(value => objectMapper.readValue[Array[RootObjectTNER]](value)).recover {
      case _: FileNotFoundException =>
        Platform.runLater(ShowDialog.showError(DialogStrings.checkEnteredDateAndTryAgain,
          DialogStrings.noQuotesFoundForGivenParameters))
        null
      case _: IOException =>
        Platform.runLater(ShowDialog.showError(DialogStrings.reduceRangeOfDaysAndTryAgain,
          DialogStrings.rangeDays93Limit))
        null
      case _: SocketTimeoutException =>
        Platform.runLater(ShowDialog.showError(DialogStrings.tryAgain,
          DialogStrings.connectionTimeout))
        null
    }
    Await.result(eventualRootObjectTNERArray, 5000 milliseconds)
  }

  @throws(classOf[IOException])
  @throws(classOf[SocketTimeoutException])
  def getContentFromUrl(url: String,
                        connectTimeout: Int = 5000,
                        readTimeout: Int = 5000,
                        requestMethod: String = "GET"): Future[String] = Future {
    import java.net.{HttpURLConnection, URL}
    val connection = new URL(url).openConnection.asInstanceOf[HttpURLConnection]
    connection.setConnectTimeout(connectTimeout)
    connection.setReadTimeout(readTimeout)
    connection.setRequestMethod(requestMethod)
    val inputStream = connection.getInputStream
    val content = io.Source.fromInputStream(inputStream).mkString
    if (inputStream != null) inputStream.close()
    content
  }
}
