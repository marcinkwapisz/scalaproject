package Service

import scalafx.scene.control.Alert
import scalafx.scene.control.Alert.AlertType

object ShowDialog {
  def showWarning(inputContentText: String, inputHeaderText: String): Unit = {
    new Alert(AlertType.Warning) {
      title = "Ostrzeżenie"
      headerText = inputHeaderText
      contentText = inputContentText
    }.showAndWait()
  }

  def showWarningAboutDate: String => Unit = showWarning("Proszę wybrać poprawną datę.", _: String)

  def showError(inputContentText: String, inputHeaderText: String): Unit = {
    new Alert(AlertType.Error) {
      title = "Błąd"
      headerText = inputHeaderText
      contentText = inputContentText
    }.showAndWait()
  }
}
